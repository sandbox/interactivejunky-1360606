(function ($) {
  
  // Store our function as a property of Drupal.behaviors.
  Drupal.behaviors.issue_list = {
    attach: function (context, settings) {
            
        /**
        * Go to elements by clicking on the issue row
        */
        $('#edit-issue-table tbody tr td.element-column',context).click(function(event){
          
          // Prevent propogation
          event.stopPropagation();
          
          // Find the element
          var $issueElement = $($(this).text());
          var $issuePath = $(this).attr('issue_path');
          
          // Close the issue list fieldset
          Drupal.toggleFieldset('#issue-list-main');
          
          // If the element can be found by jQuery, send the user to the element and highlight it
          if($issueElement.size()>0){
            $('html, body').animate({
            	scrollTop: $issueElement.offset().top - 100
            }, 200,function(){
              
            });
            $issueElement.addClass('issue-list-highlight');
            Drupal.behaviors.issue_list.displayIssuePopup($issueElement,$issuePath );
          }
          
          
        })
        
        /**
        * When clicking the element selector button in the form, run the element selector function
        */
        
        $('#element-select',context).click(function(e){
          
          Drupal.behaviors.issue_list.elementSelector();
          return false;
          
        });
        
        /**
        * Get the users window size and add it to the issue on submit
        */
        
        $(window).resize(function(){
          $('#screen-settings-input').val($(window).width() + ' x ' + $(window).height())
        })
        
        $(window).resize();
        
        /**
        * Automatically update issue table when ticked
        */
        $('#edit-issue-table input:checkbox',context).click(function(){
          // Ensures that the select all function fires before this one
          $(this).delay(10).queue(function() {
            $('input[name="mark_issues_complete"]').mousedown();
          });
        })
        
        /** 
        * Enable live sorting on tables
        */
        
        // Get the issue table
        var $issue_table = $('#edit-issue-table table',context);
        
        // Prevent non-js table sorting from executing
        $('th a',$issue_table).click(function(e){
          e.preventDefault();
        })
    },
    elementSelector : function(){
      
      /**
      * Element selector
      * This function allows the users to select any div on the page and return it into the issue list form element text field
      */
      
      // The elements we're selecting
      var $divs = $('div');
      
      $divs.mouseover(function(event){
        event.stopPropagation(); // Prevents parent elements from being selected
        $(this).addClass('issue-list-highlight') // Add an outline
      }).mouseout(function(event){
        event.stopPropagation(); // Prevents parent elements from being selected
        $(this).removeClass('issue-list-highlight') // Remove the outline
      }).click(function(event){
        event.stopPropagation(); // Prevents parent elements from being selected
        event.preventDefault(); // Stops any click events from occuring (eg links)
        $(this).removeClass('issue-list-highlight') // Remove the outline
        // Get the element's jQuery selector property, either by it's id, or by it's parent id, class and index
        var _elem = ($(this).attr('id')) ? '#' + $(this).attr('id') : '#' + $(this).closest('div[id]').attr('id') + ' .' + $(this).attr('class').split(' ').join('.');
        
        // Add the selector value to the form field
        $('#element-field').val(_elem);
        
        // Unvind the element elector events and remove outlines
        // @todo: Make it so that it only unbinds the element selector functions and not any other jQuery
        $divs.unbind().not(this).css('outline','');
      })
      
      
    },
    displayIssuePopup : function(elem,issue_url){
      
      // Close any existing issue popups
      $('.issue-list-popup').click();
      
      // Create a new issue popup, add it to the body, give it a click that removes it, and load the issue into it
      $('<div/>',{
        class:'issue-list-popup',
        text:'...',
        css:{
          'top':elem.offset().top - 20,
          'left':elem.offset().left - 20
        }
      }).appendTo('body').click(function(){
        $(this).remove();
        $('.issue-list-highlight').removeClass('issue-list-highlight');
      }).load(issue_url);
    }
  }

}(jQuery));