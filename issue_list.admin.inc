<?php

/**
 * @file
 * Admin page for the issue list module
 */

/**
 * Administration settings
 */
function issue_list_admin($form, &$form_state) {
  
  // Get the current set variables
   /*$paths       = variable_get('issue_list_paths','');
   $options     = variable_get('queryloader_options');
   $visibility  = variable_get('queryloader_visibility',QUERYLOADER_VISIBILITY_NOTLISTED);
   $types       = variable_get('queryloader_types');*/
  
  $form['ui']['button_position'] = array(
    '#type' => 'select',
    '#multiple' => FALSE,
    '#title' => t('Button position'),
    '#description' => 'Where on the page the issue list button will sit',
    '#options' => array(
      'br' => t('Bottom right (default)'),
      'bl' => t('Bottom left'),
      'tl' => t('Top left'),
      'tr' => t('Top right'),
    ),
  );
  $form['ui']['open_style'] = array(
    '#type' => 'select',
    '#multiple' => FALSE,
    '#title' => t('Opening style'),
    '#options' => array(
      'horizontal' => t('Horizontal (default)'),
      'vertical' => t('Vertical sidebar'),
    ),
  );
  $form['ui']['issue_tester'] = array(
    '#type' => 'fieldset',
    '#title' => t('Testers'),
    '#description' => t('User interface for testers'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['ui']['issue_tester']['fields'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Database fields'),
    '#options' => array(
      'grow' => t('Grow'),
      'fade' => t('Fade'),
    ),
  );
  $form['script_settings']['deep_search'] = array(
    '#type' => 'radios',
    '#title' => t('Deep Search'),
    '#description' => t('Enable to find ALL images with the selected elements. If you don\'t want queryLoader to look in the children, disable.'),
    '#options' => array(
      1 => 'Enabled',
      0 => 'Disabled'
    ),
  );
  $form['script_settings']['percentage'] = array(
    '#type' => 'radios',
    '#title' => t('Percentage visualising'),
    '#description' => t('Enable to see percentage text on the loading screen.'),
    '#options' => array(
      1 => 'Enabled',
      0 => 'Disabled',
    ),
  );
  $form['script_settings']['on_load_complete'] = array(
    '#type' => 'textfield',
    '#title' => t('onLoadComplete function'),
    '#description' => t('This function is called once the loading is complete. This is handy when changing the animation at the end.'),
    '#size' => 40,
    '#maxlength' => 255,
  );
  $form['script_settings']['on_complete'] = array(
    '#type' => 'textfield',
    '#title' => t('onComplete function'),
    '#description' => t('This function is called once the loading and animation are completed.'),
    '#size' => 40,
    '#maxlength' => 255,
  );
  
  return $form;
}